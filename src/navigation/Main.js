import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import BookDetail from "../screens/UserScreen/book/BookDetail";
import LogIn from "../screens/login/LogIn";
import SignUp from "../screens/login/SignUp";
import HomeScreenUser from "../screens/UserScreen/home/HomeScreenUser";
import UserInfo from "../screens/UserScreen/user/include/UserInfo";
import EditInfo from "../screens/UserScreen/user/include/EditInfo";
import ChangePassword from "../screens/UserScreen/user/include/ChangePassword";
import ForgotPassword from "../screens/login/ForgotPassword";
import ForgotPasswordChange from "../screens/login/ForgotPasswordChange";
import { useState } from "react";
import authApi from "../api/AuthService";
import { useEffect } from "react";
import HomeScreenAdmin from "../screens/AdminScreen/home/HomeScreenAdmin";
import BookDetailAdmin from "../screens/AdminScreen/book/BookDetailAdmin";
import BookEdit from "../screens/AdminScreen/book/BookEdit";
import userDetailAdmin from "../screens/AdminScreen/UserList/UserDetailAdmin";
import Borrowing from "../screens/UserScreen/user/include/Borrowing";
import Borrowed from "../screens/UserScreen/user/include/Borrowed";
import BookRequest from "../screens/UserScreen/user/include/BookRequest";
import BorrowOoD from "../screens/UserScreen/user/include/BorrowOoD";

const Stack = createStackNavigator();

function NavigationIndex() {
    const [user, setUser] = useState(true);
    const [admin, setAdmin] = useState(false);

    const getUser = async () => {
        try {
            const response = await authApi.getUser();
            if (response.status === 200) {
                if (
                    response.data.user_type != 2 &&
                    response.data.user_type == 1
                ) {
                    setUser(false);
                    setAdmin(true);
                    console.log("------------", response.data.user_type);
                }
            }
        } catch {
            console.log("E");
        }
    };

    useEffect(() => {
        getUser();
    }, []);

    return (
        <Stack.Navigator
            initialRouteName="HomeScreenUser"
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen
                name={admin == true ? "HomeScreenAdmin" : "HomeScreenUser"}
                component={admin == true ? HomeScreenAdmin : HomeScreenUser}
            />
            <Stack.Screen name="LogIn" component={LogIn} />
            <Stack.Screen name="SignUp" component={SignUp} />
            {user === true && (
                <>
                    <Stack.Screen name="BookDetail" component={BookDetail} />
                    <Stack.Screen name="UserInfo" component={UserInfo} />
                    <Stack.Screen name="EditInfo" component={EditInfo} />
                    <Stack.Screen
                        name="ChangePassword"
                        component={ChangePassword}
                    />
                    <Stack.Screen
                        name="ForgotPassword"
                        component={ForgotPassword}
                    />
                    <Stack.Screen
                        name="ForgotPasswordChange"
                        component={ForgotPasswordChange}
                    />
                    <Stack.Screen
                        name="Borrowing"
                        component={Borrowing}
                    />
                    <Stack.Screen
                        name="Borrowed"
                        component={Borrowed}
                    />
                    <Stack.Screen
                        name="BorrowOoD"
                        component={BorrowOoD}
                    />
                    <Stack.Screen
                        name="Request"
                        component={BookRequest}
                    />
                </>
            )}
            {admin === true && (
                <>
                    <Stack.Screen
                        name="BookDetailAdmin"
                        component={BookDetailAdmin}
                    />
                    <Stack.Screen name="BookEdit" component={BookEdit} />
                    <Stack.Screen
                        name="userDetailAdmin"
                        component={userDetailAdmin}
                    />
                </>
            )}
        </Stack.Navigator>
    );
}

export default NavigationIndex;
