import axiosClient from "./axiosClient";

const bookApi = {
    async getAllBook() {
        try {
            const url = `?controller=Public&action=get_all_book`;
            const response = await axiosClient.get(url);

            if (response.status === 200) {
                return response.data;
            }
        } catch (e) {
            console.log("-----------------------------Error", e);
        }
    },

    async getBookById(id) {
        try {
            const url = `?controller=Public&action=get_book_by_id&id_book=${id}`;
            const response = await axiosClient.get(url);

            if (response.status === 200) {
                return response.data;
            }
        } catch (e) {
            console.log("~~~~~~~~~~~~~~~~~~~~~Errp");
        }
    },

    async reserveBook(data) {
        try {
            const url = `?controller=BookStartBefore&action=book_started_before`;
            const response = await axiosClient.post(url, data);

            if (response.status === 201) {
                console.log("---------------Dat sach thanh cong!");
                return response;
            } else {
                return response;
            }
        } catch (e) {
            console.log("----------------Dat sach that bai!");
        }
    },

    async getBookHistory() {
        try {
            const url = `?controller=BorrowBook&action=get_history_borrow_book_by_id_user`;
            const response = await axiosClient.get(url);

            if (response.status == 200) {
                return response.data;
            }
        } catch (e) {
            console.log("-----------------------------Error", e);
        }
    },
    
    async updateBook(data) {
        try {
            const url = `?controller=Book&action=update_book`;
            const response = await axiosClient.put(url, data);

            if (response.status == 200 || response.status == 201) {
                console.log("Update thanh cong");
                return response.data;
            } else {
                return response.data;
            }
        } catch (e) {
            console.log("Update that bai ", e);
        }
    },

    async deleteBookById(id) {
        console.log(id);
        try {
            const url = `?controller=Book&action=delete_book&id_book=${id}`;
            const response = await axiosClient.delete(url);
            if (response.status == 200) {
                console.log("Xoá thành công !");
                return response;
            }
        } catch (e) {
            console.log("~~~~~~~~~~~~~~~~~~~~~Errp");
        }
    },

    async requestBorrow(data) {
        try {
            const url = `?controller=BorrowBook&action=borrow_book`;
            const response = await axiosClient.post(url, data);

            if (response.status === 200) {
                return response.data;
            }
        } catch (e) {
            console.log("-----------------------------Error", e);
        }
    },

    async requestLend(data) {
        try {
            const url = `?controller=BorrowBook&action=give_book_back`;
            const response = await axiosClient.post(url, data);

            if (response.status === 200) {
                return response.data;
            }
        } catch (e) {
            console.log("-----------------------------Error", e);
        }
    },
};

export default bookApi;
