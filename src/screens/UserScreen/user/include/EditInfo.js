import { View, Text, TextInput, StyleSheet, TouchableOpacity, Picker, Image } from 'react-native';
import React, { useState, useEffect } from 'react';
import authApi from '../../../../api/AuthService';

function EditName({ navigation }) {

    const [firstname, setFirstname] = useState();
    const [lastname, setLastname] = useState();
    const [day, setDay] = useState();
    const [month, setMonth] = useState();
    const [year,setYear] = useState();
    const [sex, setSex] = useState();
    const [email, setEmail] = useState();
    const [address, setAddress] = useState();
    const [phone, setPhone] = useState();
    const [alert, setAlert] = useState('');
    const [noti1, setNoti1] = useState(false);
    const [noti2, setNoti2] = useState(false);
    
    const getUsername = async () => {
        const response = await authApi.getUser();
        setFirstname(response.data.firstname);
        setLastname(response.data.lastname);
        setDay(response.data.day);
        setMonth(response.data.month);
        setYear(response.data.year);
        setSex(response.data.sex);
        setEmail(response.data.email);
        setPhone(response.data.phone_number);
        setAddress(response.data.address);
    }

    useEffect(() => {
        getUsername();
    }, []);

    const update = async () => {
        try {
            let data = { firstname: firstname, lastname: lastname, email: email, 
                        phone_number: phone, address: address, sex: sex, 
                        month: month, day: day, year: year}
            const response = await authApi.changeUserInfo(data);
            if (response.status === 200) {
                console.log('Update Success!')
                setNoti1(true);
                setAlert('Update Successfully!');
            }
        } catch {
            setNoti2(true);
            setAlert('Update Unsuccessfully!')
        }
        
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: 'row', marginLeft: 5,}}>
                        <Image style={styles.imageBack} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png' }}/>
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <Text style={styles.titleInput}>Name</Text>
                <View style={styles.name}>
                    <TextInput style={styles.inputName} value={firstname || ''} onChangeText={(text) => setFirstname(text)} placeholderTextColor='grey' placeholder='First Name' />
                    <TextInput style={styles.inputName} value={lastname || ''} onChangeText={(text) => setLastname(text)} placeholderTextColor='grey' placeholder='Last Name' />
                </View>
                
                <Text style={styles.titleInput}>Date of Birth</Text>
                <View style={styles.dob}>
                    <TextInput style={styles.inputDob} value={day || ''} onChangeText={(text) => setDay(text)} placeholderTextColor='grey' placeholder='dd' />
                    <TextInput style={styles.inputDob} value={month || ''} onChangeText={(text) => setMonth(text)} placeholderTextColor='grey' placeholder='mm' />
                    <TextInput style={styles.inputDob} value={year || ''} onChangeText={(text) => setYear(text)} placeholderTextColor='grey' placeholder='yy' />
                </View>

                <Text style={styles.titleInput}>Email</Text>
                <TextInput style={styles.input} value={email || ''} onChangeText={(text)=> setEmail(text)} placeholderTextColor='grey' placeholder='Email' />

                <Text style={styles.titleInput}>Phone Number</Text>
                <TextInput style={styles.input} value={phone || ''} onChangeText={(text) => setPhone(text)} placeholderTextColor='grey' placeholder='Phone Number' />

                <Text style={styles.titleInput}>Sex</Text>
                <Picker style={styles.input} selectedValue={sex} onValueChange={(item) => setSex(item)}>
                    <Picker.Item label="Male" value="Male" />
                    <Picker.Item label="Female" value="Female" />
                    <Picker.Item label="Other" value="Other" />
                </Picker>

                <Text style={styles.titleInput}>Address</Text>
                <TextInput style={styles.input} value={address || ''} onChangeText={(text) => setAddress(text)} placeholderTextColor='grey' placeholder='Address' />
                
                { noti1 && <Text style={styles.alert1}>{alert}</Text> }
                { noti2 && <Text style={styles.alert2}>{alert}</Text> }
                <TouchableOpacity style={styles.btn} onPress={update}><Text>Save</Text></TouchableOpacity>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#efefef',
    },
    content: {
        backgroundColor: 'white',
        padding: 10,
        paddingTop: 20,
    },
    titleInput: {
        marginLeft: 10,
        marginBottom: 5,
    },
    input: {
        padding: 10,
        marginHorizontal: 10,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#D3D3D3',
        borderRadius: 8,
    },
    btn: {
        textAlign: 'center',
        backgroundColor: '#007EFF',
        padding: 10,
        width: '95%',
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 20,
        color: 'white'
    },
    dob: {
        flexDirection: 'row',
    },
    inputDob: {
        width: '30%',
        borderWidth: 1,
        borderColor: '#D3D3D3',
        borderRadius: 8,
        padding: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    name: {
        flexDirection: 'row',
    },
    inputName: {
        width: '50%',
        borderWidth: 1,
        borderColor: '#D3D3D3',
        borderRadius: 8,
        padding: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    alert1: {
        color: '#00ff00',
        textAlign: 'center',
        marginTop: 5,
    },
    alert2: {
        color: '#ff3300',
        textAlign: 'center',
        marginTop: 5,
    },
    header: {
        fontSize: 24,
        textAlign: 'center',
        fontWeight: 600,
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#3440FF',
    },
    title: {
        textAlign: 'center',
        width: '60%',
        fontSize: 17,
    },
    back: {
        width: '20%',
        textAlign: 'left',
        marginLeft: 10,
    },
    button: {
        width: '20%',
        textAlign: 'right',
        marginRight: 10,
    },
    imageBack: {
        width: 24,
        height: 24,
    }
})

export default EditName;