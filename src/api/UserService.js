import axiosClient from "./axiosClient";

const userApi = {
    async getAllUser() {
        try {
            const url = `?controller=User&action=get_all_user`;
            const response = await axiosClient.get(url);
            if (response.status === 200) {
                return response.data;
            } else {
                return response.data;
            }
        } catch (e) {
            console.log("--------------- profile@E ", e);
        }
    },
    async getUserById(id) {
        try {
            const url = `?controller=User&action=get_user_by_id&id_user=${id}`;
            const response = await axiosClient.get(url);

            if (response.status === 200) {
                return response.data;
            }
        } catch (e) {
            console.log("~~~~~~~~~~~~~~~~~~~~~Errp");
        }
    },
    async deleteUserById(id) {
        try {
            const url = `?controller=User&action=delete_user_by_id&id_user=${id}`;
            const response = await axiosClient.delete(url);

            if (response.status === 200) {
                console.log("Xoá thành công !");
                return response.data;
            }
        } catch (e) {
            console.log("~~~~~~~~~~~~~~~~~~~~~Errp");
        }
    },
};

export default userApi;
