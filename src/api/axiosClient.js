import axios from "axios";

const axiosClient = axios.create({
    baseURL: "http://localhost/MVC_book/routing.php",
    headers: {
        "Content-Type": "application/json",
    },
    withCredentials: false,
});

if (localStorage.getItem("accessToken"))
    axiosClient.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("accessToken");

export default axiosClient;
