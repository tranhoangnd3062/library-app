import axiosClient from "./axiosClient";

const authApi = {
    async getUser() {
        try {
            const url = `?controller=User&action=get_user_by_id`;
            const response = await axiosClient.get(url);
            console.log("----------- profile: ", response);
            if (response.status == 200) {
                return response.data;
            } else {
                return response.data;
            }
        } catch (e) {
            console.log("--------------- profile@E ", e);
        }
    },

    async login(data) {
        try {
            const url = `?controller=Authentication&action=login`;
            const response = await axiosClient.post(url, data);
            console.log("----------- login: ", response);
            if (response.status === 200 || response.status === 201) {
                return response.data;
            } else {
                return response.data;
            }
        } catch (e) {
            console.log("--------------- login@E ", e);
        }
    },

    async register(data) {
        try {
            const url = `?controller=Authentication&action=register`;
            const response = await axiosClient.post(url, data);

            if (response.status === 200 || response.status === 201) {
                console.log("Dang ky thanh cong");
                return response.data;
            } else {
                return response.data;
            }
        } catch (e) {
            console.log("Dang ky that bai ", e);
        }
    },

    async changePassword(data) {
        try {
            const url = `?controller=User&action=change_password`;
            const response = await axiosClient.put(url, data);

            if (response.status === 200 || response.status === 201) {
                console.log("Doi mat khau thanh cong");
                return response.data;
            } else {
                return response.data;
            }
        } catch (e) {
            console.log("Doi mat khau that bai ", e);
        }
    },

    async changeUserInfo(data) {
        try {
            const url = `?controller=User&action=update_user`;
            const response = await axiosClient.put(url, data);

            if (response.status === 200 || response.status === 201) {
                console.log("Doi thong tin thanh cong");
                return response.data;
            }
        } catch (e) {
            console.log("Doi thong tin that bai ", e);
        }
    },

    async forgetPassword(data) {
        try {
            const url = `?controller=Public&action=forget_password`;
            const response = await axiosClient.post(url, data);

            if (response.status === 200) {
                console.log("Xac thuc thanh cong");
                return response.data;
            }
        } catch (e) {
            console.log("Xac thuc that bai", e);
        }
    },

    async forgetPasswordChange(data) {
        try {
            const url = `?controller=User&action=change_password_for_forget_password`;
            const response = await axiosClient.put(url, data);

            if (response.status === 200) {
                console.log("Doi mat khau thanh cong");
                return response.data;
            }
        } catch (e) {
            console.log("Doi mat khau that bai", e);
        }
    },
};

export default authApi;
