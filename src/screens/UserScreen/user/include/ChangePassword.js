import { useEffect, useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, Image } from 'react-native';
import authApi from '../../../../api/AuthService';

function ChangePassword({navigation}) {

    const [currentPassword, setCurrentPassword] = useState();
    const [newPassword, setNewPassword] = useState();
    const [confirmPassword, setConfirmpassword] = useState();
    const [noti, setNoti] = useState(false);
    const [hide, setHide] = useState(true);
    const [alert, setAlert] = useState(false);

    const handleChangePassword = async () => {
        try {
            let data = {
                old_password: currentPassword,
                new_password: newPassword,
                confirm_new_password: confirmPassword,
            }

            const response = await authApi.changePassword(data);
            if (response.status === 200) {
                if (noti === true) {
                    setCurrentPassword('');
                    setNewPassword('');
                    setConfirmpassword('');
                }
            } 
        } catch {
            setAlert(true);
        }
        
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: 'row', marginLeft: 5,}}>
                        <Image style={styles.imageBack} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png' }}/>
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <Text style={styles.title}>Change Password</Text>

                <TextInput
                    style={styles.input}
                    placeholderTextColor="grey"
                    placeholder="Curent Password"
                    value={currentPassword}
                    onChangeText={(text) => setCurrentPassword(text)}
                    secureTextEntry={hide === true ? 'true' : 'false'}
                />

                <TextInput
                    style={styles.input}
                    placeholderTextColor="grey"
                    placeholder="New Password"
                    value={newPassword}
                    onChangeText={(text) => setNewPassword(text)}
                    secureTextEntry={hide === true ? 'true' : 'false'}
                />

                <TextInput
                    style={styles.input}
                    placeholderTextColor="grey"
                    placeholder="Confirm New Password"
                    value={confirmPassword}
                    onChangeText={(text) => setConfirmpassword(text)}
                    secureTextEntry={hide === true ? 'true' : 'false'}
                />

                <TouchableOpacity style={styles.button} onPress={() => {handleChangePassword(); setNoti(true)}}><Text>Save</Text></TouchableOpacity>
            </View>
            
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#efefef',
    },
    content: {
        backgroundColor: 'white',
        padding: 10,
    },
    title: {
        fontSize: 24,
        padding: 10,
        textAlign: 'center'
    },
    input: {
        padding: 10,
        margin: 10,
        borderWidth: 1,
        borderColor: '#D3D3D3',
        borderRadius: 8,
    },
    button: {
        textAlign: 'center',
        backgroundColor: '#007EFF',
        padding: 10,
        width: '95%',
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 20,
        color: 'white'
    },
    header: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#3440FF',
    },
    imageBack: {
        width: 24,
        height: 24,
    }
})

export default ChangePassword;