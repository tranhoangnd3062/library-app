import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Modal,
} from "react-native";
import userApi from "../../../api/UserService";

function UserList({ navigation }) {
    const [user, setUser] = useState([]);
    const [input, setInput] = useState("");
    const [alert, setAlert] = useState(false);
    const [alert2, setAlert2] = useState(false);

    async function handleDelete(id) {
        try {
            const response = await userApi.deleteUserById(id);
            if (response.status == 200) {
                setAlert(true);
            }
        } catch (e) {
            setAlert2(true);
        }
    }

    async function getUser() {
        const response = await userApi.getAllUser();
        setUser(response.data.body);
    }

    function removeVietnameseTones(str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");
        // Some system encode vietnamese combining accent as individual utf-8 characters
        // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
        // Remove extra spaces
        // Bỏ các khoảng trắng liền nhau
        str = str.replace(/ + /g, " ");
        str = str.trim();
        // Remove punctuations
        // Bỏ dấu câu, kí tự đặc biệt
        str = str.replace(
            /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
            " "
        );
        return str;
    }

    useEffect(() => {
        getUser();
    }, []);

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.titlecontent}>
                    <Text style={styles.title}>All Users</Text>
                </View>
                <View style={styles.search}>
                    <TextInput
                        style={styles.input}
                        placeholder="Search username"
                        value={input}
                        onChangeText={(text) => {
                            setInput(text);
                        }}
                    />
                    <Image
                        style={styles.logo}
                        source={require("../../../../assets/magnify.png")}
                    />
                </View>
                <View>
                    {input
                        ? user
                              .filter((item) =>
                                  removeVietnameseTones(item.user_name)
                                      .toLowerCase()
                                      .match(
                                          removeVietnameseTones(
                                              input.toLowerCase()
                                          )
                                      )
                              )
                              .map((item, index) => (
                                  <View key={index}>
                                      <TouchableOpacity
                                          onPress={() =>
                                              navigation.push(
                                                  "userDetailAdmin",
                                                  { id: item.id }
                                              )
                                          }
                                      >
                                          <View style={styles.content}>
                                              <View style={styles.detail}>
                                                  
                                                    <Text style={styles.text1}>
                                                        ID: {item.id}
                                                    </Text>
                                                      <Text
                                                          style={styles.text1}
                                                      >
                                                          Username:{" "}
                                                          {item.user_name}
                                                      </Text>
                                                
                                                  <Text style={styles.text1}>
                                                      Name:{" "}
                                                      {item.firstname +
                                                          " " +
                                                          item.lastname}
                                                  </Text>
                                                  
                                                  <Text
                                                      style={[
                                                          styles.text1,
                                                          { color: "blue" },
                                                      ]}
                                                  >
                                                      {item.user_type == 1
                                                          ? "Admin"
                                                          : "Customer"}
                                                  </Text>

                                                  <View
                                                      style={{
                                                          flexDirection:
                                                              "column",
                                                          marginTop: 20,
                                                      }}
                                                  >
                                                      <TouchableOpacity
                                                          style={{
                                                              backgroundColor:
                                                                  "#E57878",
                                                              borderRadius: 5,
                                                              marginBottom: 10,
                                                              padding: 5,
                                                              width: 60,
                                                              marginRight: 5,
                                                              alignSelf: 'flex-end',
                                                          }}
                                                          onPress={() =>
                                                              handleDelete(
                                                                  item.id
                                                              )
                                                          }
                                                      >
                                                          <Text
                                                              style={{
                                                                  textAlign:
                                                                      "center",
                                                                  color: "white",
                                                                  fontWeight: 500,
                                                              }}
                                                          >
                                                              Delete
                                                          </Text>
                                                      </TouchableOpacity>
                                                      <Text
                                                          style={styles.status}
                                                      >
                                                          {item.status}
                                                      </Text>
                                                  </View>
                                              </View>
                                          </View>
                                      </TouchableOpacity>
                                  </View>
                              ))
                        : user.map((item, key) => (
                              <View key={key}>
                                  <TouchableOpacity
                                      onPress={() =>
                                          navigation.push("userDetailAdmin", {
                                              id: item.id,
                                          })
                                      }
                                  >
                                      <View style={styles.content}>
                                          <View style={styles.detail}>
                                            <Text style={styles.text1}>
                                                  ID: {item.id}
                                              </Text>
                                            <Text style={styles.text1}>
                                                Username: {item.user_name}
                                            </Text>
                           
                                              <Text style={styles.text1}>
                                                  Name:{" "}
                                                  {item.firstname +
                                                      " " +
                                                      item.lastname}
                                              </Text>
                                              
                                              <Text
                                                  style={[
                                                      styles.text1,
                                                      { color: "blue" },
                                                  ]}
                                              >
                                                  {item.user_type == 1
                                                      ? "Admin"
                                                      : "Customer"}
                                              </Text>
                                              <View
                                                  style={{
                                                      flexDirection: "column",
                                                      marginTop: 20,
                                                  }}
                                              >
                                                  <TouchableOpacity
                                                      style={{
                                                          backgroundColor:
                                                              "#E57878",
                                                          borderRadius: 5,
                                                          marginBottom: 10,
                                                          padding: 5,
                                                          width: 100,
                                                          marginRight: 5,
                                                          alignSelf: 'flex-end',
                                                      }}
                                                      onPress={() =>
                                                          handleDelete(item.id)
                                                      }
                                                  >
                                                      <Text
                                                          style={{
                                                              textAlign:
                                                                  "center",
                                                              color: "white",
                                                              fontWeight: 500,
                                                              fontSize: 15,
                                                          }}
                                                      >
                                                          Delete
                                                      </Text>
                                                  </TouchableOpacity>
                                                  <Text style={styles.status}>
                                                      {item.status}
                                                  </Text>
                                              </View>
                                          </View>
                                      </View>
                                  </TouchableOpacity>
                              </View>
                          ))}
                    <Modal transparent={true} visible={alert}>
                        <View style={styles.popup}>
                            <View style={styles.popupContent2}>
                                <Text
                                    style={{
                                        fontSize: 18,
                                        textAlign: "center",
                                        color: "green",
                                    }}
                                >
                                    Delete Successfully!
                                </Text>
                                <View style={{ justifyContent: "center" }}>
                                    <TouchableOpacity
                                        style={styles.buttonCancel}
                                        onPress={() => setAlert(false)}
                                    >
                                        <Text style={styles.textCancel}>
                                            Close
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    <Modal transparent={true} visible={alert2}>
                        <View style={styles.popup}>
                            <View style={styles.popupContent2}>
                                <Text
                                    style={{
                                        fontSize: 18,
                                        textAlign: "center",
                                        color: "red",
                                    }}
                                >
                                    Delete Unsuccessfully!
                                </Text>
                                <View style={{ justifyContent: "center" }}>
                                    <TouchableOpacity
                                        style={styles.buttonCancel}
                                        onPress={() => setAlert2(false)}
                                    >
                                        <Text style={styles.textCancel}>
                                            Close
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },

    title: {
        justifyContent: "center",
        fontSize: 24,
        textAlign: "center",
        backgroundColor: "#AAC9E8",
        padding: 15,
        fontWeight: 600,
    },
    filter: {
        flexDirection: "row",
        marginBottom: 5,
        marginTop: 30,
        justifyContent: "center",
    },
    checkbox: {
        marginLeft: 30,
    },
    label: {
        marginLeft: 10,
        marginRight: 20,
    },
    content: {
        flex: 1,
        marginTop: 10,
        flexDirection: "row",
        width: "100%",
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        backgroundColor: "white",
        paddingBottom: 10,
    },
    image: {
        width: 100,
        height: 150,
        padding: 5,
        margin: 10,
    },
    detail: {
        flex: 1,
        padding: 5,
    },
    name: {
        flex: 1,
        fontSize: 18,
        marginLeft: 10,
        marginBottom: 10,
    },
    text1: {
        marginLeft: 10,
        marginBottom: 2,
        fontSize: 20,
    },
    search: {
        fleX: 1,
        backgroundColor: "white",
        // marginBottom: 15,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        flexDirection: "row",
        justifyContent: "center",
    },
    input: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: "black",
        padding: 10,
        borderRadius: 15,
        marginTop: 20,
        width: "80%",
    },
    searchbtn: {
        padding: 5,
        alignSelf: "center",
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 5,
        width: 70,
    },
    searchbtn_text: {
        textAlign: "center",
    },
    logo: {
        width: 20,
        height: 20,
        top: "41%",
        right: "9%",
    },
    status: {
        color: "green",
        fontSize: 14,
        textAlign: 'right',
        marginTop: 10,
        marginRight: 10,
    },
    buttonREST: {
        textAlign: "center",
        top: -55,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "dark",
        padding: 7,
        minWidth: 50,
        maxWidth: 100,
    },
    popup: {
        flex: 1,
        backgroundColor: "#000000aa",
    },
    popupContent2: {
        justifyContent: "center",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 10,
        marginHorizontal: 30,
        marginVertical: "80%",
    },
    textCancel: {
        color: "white",
    },
    buttonCancel: {
        marginTop: 20,
        alignSelf: "center",
        width: 100,
        backgroundColor: "#59ACFF",
        borderRadius: 10,
        padding: 10,
        textAlign: "center",
        fontSize: 18,
        marginBottom: 10,
        marginLeft: 10,
    },
});

export default UserList;
