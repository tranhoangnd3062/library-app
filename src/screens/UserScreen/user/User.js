import { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import authApi from "../../../api/AuthService";
import { MaterialCommunityIcons } from '@expo/vector-icons';

function User({ navigation }) {
    const [isUser, setIsUser] = useState(false);
    const [username, setUsername] = useState("");
    const [userType, setUserType] = useState();

    const getUser = async () => {
        if (localStorage.getItem("accessToken") != null) {
            const response = await authApi.getUser();
            if (response.status == 200) {
                setUsername("USER" + response.data.id);
                setUserType(response.data.user_type);
                setIsUser(true);
                console.log("GetProfile Succes!");
                console.log(response.data.id);
            }
        } else {
            setIsUser(false);
        }
    };

    useEffect(() => {
        getUser();
    }, []);

    function Logout() {
        localStorage.clear();
        window.location.reload();
        navigation.replace("HomeScreen");
    }

    return (
        <View style={styles.container}>
            <View style={styles.user}>
                {isUser == true ? (
                    <>
                        <TouchableOpacity style={styles.info}>
                            <Image
                                style={styles.avatar}
                                source={require("../../../../assets/user.png")}
                            />
                            <Text style={styles.name}>{username}</Text>
                        </TouchableOpacity>
                        {userType == 1 ? (
                            <View style={styles.list}>
                                <TouchableOpacity
                                    style={styles.tablist2}
                                    onPress={Logout}
                                >
                                    <MaterialCommunityIcons
                                        name="logout-variant"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                        ) : (
                            <View style={styles.list}>
                                <TouchableOpacity
                                    style={styles.tablist1}
                                    onPress={() => navigation.push("UserInfo")}
                                >
                                    <MaterialCommunityIcons
                                        name="account-edit"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>User Information</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.tablist1}
                                    onPress={() =>
                                        navigation.push("ChangePassword")
                                    }
                                >
                                    <MaterialCommunityIcons
                                        name="lock-reset"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>Change Password</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.tablist2} onPress={() => navigation.push('Borrowing')}>
                                    <MaterialCommunityIcons
                                        name="format-list-bulleted"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>Book Borrowing</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.tablist1} onPress={() => navigation.push('Request')}>
                                    <MaterialCommunityIcons
                                        name="format-list-bulleted"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>Book Request</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.tablist1} onPress={() => navigation.push('Borrowed')}>
                                    <MaterialCommunityIcons
                                        name="format-list-bulleted"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>Book Borrowed</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.tablist1} onPress={() => navigation.push('BorrowOoD')}>
                                    <MaterialCommunityIcons
                                        name="format-list-bulleted"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>Book Out of Date</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.tablist2}
                                    onPress={Logout}
                                >
                                    <MaterialCommunityIcons
                                        name="logout-variant"
                                        size={20}
                                    />
                                    <Text style={{marginLeft: 10, marginTop: 1}}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    </>
                ) : (
                    <>
                        <TouchableOpacity style={styles.info}>
                            <Image
                                style={styles.avatar}
                                source={require("../../../../assets/user.png")}
                            />
                            <Text style={styles.name}>GUEST</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.tablist1} onPress={() => navigation.push('LogIn')}>
                            <MaterialCommunityIcons
                                name="account-arrow-left"
                                size={20}
                            />
                            <Text style={{marginLeft: 10, marginTop: 1}}>Login</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.tablist2}
                            onPress={() => navigation.push('SignUp')}
                        >
                            <MaterialCommunityIcons
                                name="account-plus"
                                size={20}
                            />
                            <Text style={{marginLeft: 10, marginTop: 1}}>Signup</Text>
                        </TouchableOpacity>
                    </>
                )}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#efefef",
    },
    user: {
        width: "100%",
    },
    avatar: {
        width: 40,
        height: 40,
        backgroundColor: "white",
        borderRadius: 50,
        borderColor: "#BFBFBF",
        borderWidth: 3,
        alignContent: "center",
    },
    info: {
        flexDirection: "row",
        padding: 10,
        backgroundColor: "white",
        marginBottom: 20,
        paddingTop: 20,
    },
    name: {
        marginLeft: 20,
        fontSize: 24,
        alignSelf: "center",
        textAlign: "center",
        marginBottom: 20,
    },
    list: {
        flexDirection: "column",
    },
    tablist1: {
        backgroundColor: "white",
        marginBottom: 5,
        padding: 10,
        flexDirection: 'row',
    },
    tablist2: {
        backgroundColor: "white",
        marginBottom: 5,
        padding: 10,
        marginTop: 15,
        flexDirection: 'row',
    },
});

export default User;
