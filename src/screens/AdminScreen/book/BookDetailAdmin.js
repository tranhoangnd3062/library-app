import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    Modal,
    TextInput,
} from "react-native";
import bookApi from "../../../api/BookService";

function BookDetail({ route, navigation }) {
    let { id } = route.params;
    console.log(id);
    const [book, setBook] = useState([]);
    const [show, setShow] = useState(false);
    const [alert, setAlert] = useState(false);
    const [day, setDay] = useState();
    const [month, setMonth] = useState();
    const [year, setYear] = useState();

    async function getBookById() {
        const response = await bookApi.getBookById(id);
        setBook(response.data);
    }

    useEffect(() => {
        getBookById();
    }, [id]);

    const handleReverse = async () => {
        try {
            let date = year + "-" + month + "-" + day;
            let data = { id_book: id, intend_date: date };
            const response = await bookApi.reserveBook(data);
            if (response.status === 201) {
                setShow(false);
                setAlert(true);
                console.log("Dat sach thanh cong");
            }
        } catch {
            console.log("Dat sach that bai!");
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: "row", marginLeft: 5 }}>
                        <Image
                            style={styles.imageBack}
                            source={{
                                uri: "https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png",
                            }}
                        />
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View>
                <Image style={styles.image} source={{ uri: book.book_image }} />
                <Text style={styles.title}>{book.book_name}</Text>

                <View style={styles.content}>
                    <Text style={styles.text}>Author: {book.book_author}</Text>
                    <Text style={styles.text}>
                        Language: {book.book_language}
                    </Text>
                    <Text style={styles.text}>Status: {book.status}</Text>
                    <Text style={styles.text}>
                        Description: {book.book_describe}
                    </Text>
                </View>

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => setShow(true)}
                >
                    <Text style={styles.textButton}>Reserve</Text>
                </TouchableOpacity>

                <Modal transparent={true} visible={show}>
                    <View style={styles.popup}>
                        <View style={styles.popupContent}>
                            <Text style={styles.popupTitle}>
                                When will you take this book?
                            </Text>
                            <View style={styles.popupInput}>
                                <TextInput
                                    style={styles.popupBox}
                                    placeholder="dd"
                                    placeholderTextColor="grey"
                                    value={day || ""}
                                    onChangeText={(text) => setDay(text)}
                                />
                                <TextInput
                                    style={styles.popupBox}
                                    placeholder="mm"
                                    placeholderTextColor="grey"
                                    value={month || ""}
                                    onChangeText={(text) => setMonth(text)}
                                />
                                <TextInput
                                    style={styles.popupBox}
                                    placeholder="yy"
                                    placeholderTextColor="grey"
                                    value={year || ""}
                                    onChangeText={(text) => setYear(text)}
                                />
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    justifyContent: "center",
                                }}
                            >
                                <TouchableOpacity
                                    style={styles.button}
                                    onPress={handleReverse}
                                >
                                    <Text style={styles.textButton}>
                                        Reserve
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.buttonCancel}
                                    onPress={() => setShow(false)}
                                >
                                    <Text style={styles.textCancel}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal transparent={true} visible={alert}>
                    <View style={styles.popup}>
                        <View style={styles.popupContent}>
                            <Text style={styles.popupTitle}>
                                Reserve Successfully!
                            </Text>
                            <View style={{ justifyContent: "center" }}>
                                <TouchableOpacity
                                    style={styles.buttonCancel}
                                    onPress={() => setAlert(false)}
                                >
                                    <Text style={styles.textCancel}>Đóng</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    title: {
        textAlign: "center",
        fontSize: 24,
        padding: 10,
        marginBottom: 25,
    },
    image: {
        marginTop: 20,
        width: 200,
        height: 300,
        padding: 5,
        margin: 10,
        alignSelf: "center",
    },
    text: {
        paddingLeft: 20,
        padding: 10,
        marginTop: 10,
        color: "grey",
    },
    button: {
        marginTop: 20,
        alignSelf: "center",
        width: 200,
        backgroundColor: "#cc8028",
        borderRadius: 10,
        padding: 10,
        textAlign: "center",
        fontSize: 18,
        marginBottom: 10,
    },
    buttonCancel: {
        marginTop: 20,
        alignSelf: "center",
        width: 200,
        backgroundColor: "#59ACFF",
        borderRadius: 10,
        padding: 10,
        textAlign: "center",
        fontSize: 18,
        marginBottom: 10,
        marginLeft: 10,
    },
    textCancel: {
        color: "white",
    },
    textButton: {
        color: "white",
    },
    popup: {
        flex: 1,
        backgroundColor: "#000000aa",
    },
    popupContent: {
        justifyContent: "center",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 10,
        marginHorizontal: 30,
        marginVertical: "70%",
    },
    popupTitle: {
        fontSize: 18,
        textAlign: "center",
    },
    popupInput: {
        flexDirection: "row",
        marginHorizontal: 10,
    },
    popupBox: {
        width: "30%",
        margin: 10,
        padding: 10,
        borderWidth: 1,
        borderColor: "grey",
        borderRadius: 10,
    },
    header: {
        flexDirection: "row",
        width: "100%",
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: "#3440FF",
        flex: 1,
    },
    imageBack: {
        width: 24,
        height: 24,
    },
});

export default BookDetail;
