import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import bookApi from '../../../../api/BookService';

function Borrowing({ navigation }) {

    const [bookBorrowing, setBookBorrowing] = useState([]);

    const getBook = async () => {  
        const response = await bookApi.getBookHistory();
        if (response.status === 200) {
            setBookBorrowing(response.data.borrowing_book);
        } else {
            console.log('E');
        }
    }

    useEffect(() => {
        getBook();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: 'row', marginLeft: 5,}}>
                        <Image style={styles.imageBack} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png' }}/>
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <ScrollView>
                { 
                    bookBorrowing ? bookBorrowing.map((item, key) => (
                        <View key={key}>
                            <TouchableOpacity onPress={() => navigation.push('BookDetail', { id: item.id})}>
                                <View style={styles.content} >
                                    <View styles={styles.cover}>
                                        <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                    </View>
                                    <View style={styles.detail}>
                                        <View >
                                            <Text style={styles.name}>{item.book_name}</Text>
                                        </View>
                                        <Text style={styles.text1}>Author: {item.book_author}</Text>
                                        <Text style={styles.text1}>Language: {item.book_language}</Text>
                                        <Text style={styles.text1}>ID Reserve: {item.id}</Text>
                                        <Text style={styles.status}>{item.status}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ))
                 : (
                    <View style={{flex: 1, justifyContent: 'center',alignItems: 'center', marginTop: 50}}>
                        <Image style={{width: 300, height: 300}} source={{uri: 'https://frontend.tikicdn.com/_desktop-next/static/img/account/empty-order.png'}}/>
                        <Text>Empty!</Text>
                    </View>
                )
                }
            </ScrollView>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        justifyContent: 'center',
        fontSize: 24,
        textAlign: 'center',
        backgroundColor: '#AAC9E8',
        padding: 15,
        fontWeight: 600,
    },
    filter: {
        flexDirection: "row",
        marginBottom: 5,
        marginTop: 30,
        justifyContent: 'center',
    },
    checkbox: {
        marginLeft: 30,
    },
    label: {
        marginLeft: 10,
        marginRight: 20,
    },
    content: {
        flex: 1,
        marginTop: 30,
        flexDirection: 'row',
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        backgroundColor: 'white',
        paddingBottom: 20,
    },
    image: {
        width: 100,
        height: 150,
        padding: 5,
        margin: 10,
    },
    detail: {
        flex: 1,
        padding: 5,
    },
    name: {
        flex: 1,
        fontSize: 18,
        marginLeft: 10,
        marginBottom: 10,
    },
    text1: {
        marginLeft: 10,
        marginBottom: 2,
        fontSize: 12,
    },
    status: {
        color: 'green',
        fontSize: 14,
        marginLeft: '70%',
        marginTop: 30,
    },
    header: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#3440FF',
    },
    imageBack: {
        width: 24,
        height: 24,
    }
});

export default Borrowing;
