import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import bookApi from '../../../../api/BookService';

function Home({ navigation }) {

    const [book, setBook] = useState([]);

    async function getBook() {
        const response = await bookApi.getAllBook();
        if (response.status === 200) {
            setBook(response.data);
        }

    }

    useEffect(() => {
        getBook();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.titlecontent}>
                <Text style={styles.title}>All Books</Text>
            </View>
            <ScrollView>
                <View>
                    {book.map((item, key) => (
                        <View key={key}>
                            <TouchableOpacity onPress={() => navigation.push('BookDetail', { id: item.id})}>
                                <View style={styles.content} >
                                    <View styles={styles.cover}>
                                        <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                    </View>
                                    <View style={styles.detail}>
                                        <View >
                                            <Text style={styles.name}>{item.book_name}</Text>
                                        </View>
                                        <Text style={styles.text1}>Author: {item.book_author}</Text>
                                        <Text style={styles.text1}>Language: {item.book_language}</Text>
                                        <Text style={styles.status}>{item.status}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ))}
                </View>
            </ScrollView>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        justifyContent: 'center',
        fontSize: 24,
        textAlign: 'center',
        backgroundColor: '#AAC9E8',
        padding: 15,
        fontWeight: 600,
    },
    filter: {
        flexDirection: "row",
        marginBottom: 5,
        marginTop: 30,
        justifyContent: 'center',
    },
    checkbox: {
        marginLeft: 30,
    },
    label: {
        marginLeft: 10,
        marginRight: 20,
    },
    content: {
        flex: 1,
        marginTop: 30,
        flexDirection: 'row',
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
        backgroundColor: 'white',
        paddingBottom: 20,
    },
    image: {
        width: 100,
        height: 150,
        padding: 5,
        margin: 10,
    },
    detail: {
        flex: 1,
        padding: 5,
    },
    name: {
        flex: 1,
        fontSize: 18,
        marginLeft: 10,
        marginBottom: 10,
    },
    text1: {
        marginLeft: 10,
        marginBottom: 2,
        fontSize: 12,
    },
    status: {
        color: 'green',
        fontSize: 14,
        marginLeft: '70%',
        marginTop: 30,
    }
});

export default Home;
