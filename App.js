// git config core.autocrlf true
// npm install --save react-navigation
// npm install react-native-gesture-handler react-native-reanimated react-navigation-stack
// npm install react-native-safe-area-view react-native-safe-area-context
// npm install @react-native-community/masked-view

import Main from "./src/navigation/Main";
import { NavigationContainer } from '@react-navigation/native';

export default function App() {
    return (
        <NavigationContainer>
            <Main />
        </NavigationContainer>
    );

    // return (
    //     <BookDetail />
    // )
}
