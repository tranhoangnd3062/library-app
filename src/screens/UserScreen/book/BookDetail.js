import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Modal, TextInput, ScrollView } from 'react-native';
import bookApi from '../../../api/BookService';

function BookDetail({route, navigation}) {

    let { id } = route.params;
    console.log(id)
    const [book, setBook] = useState([]);
    const [show, setShow] = useState(false);
    const [alert, setAlert] = useState(false);
    const [alertF, setAlertF] = useState(false);
    const [text, setText] = useState();
    const [day, setDay] = useState();
    const [month, setMonth] = useState();
    const [year, setYear] = useState();
    const [alertD, setAlertD] = useState(false);
    const [alertM, setAlertM] = useState(false);
    const [alertY, setAlertY] = useState(false);

    async function getBookById() {
        const response = await bookApi.getBookById(id);
        setBook(response.data);
    }

    useEffect(() => {
      getBookById();
    },[id]);

    const handleReverse = async () => {
        if (day > 32)
            setAlertD(true);
        if (year > 12) 
            setAlertM(true);
        if (year < 2023)
            setAlertY(true);
        let date = year + '-' + month + '-' + day;
        let data = { id_book: id, intend_date: date}
        const response = await bookApi.reserveBook(data);
        try {
            if (response.status === 201) {
                setShow(false);
                setAlert(true);
                console.log('Dat sach thanh cong')
            }
        } catch {
            setAlertF(true);
            setText('Reserve Failed!');
            console.log('Dat sach that bai!')
        }
        
    }
    

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: "row", marginLeft: 5, paddingBottom: 20, }}>
                        <Image
                            style={styles.imageBack}
                            source={{
                                uri: "https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png",
                            }}
                        />
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <ScrollView>
                <View>
                    <Image style={styles.image} source={{ uri: book.book_image }} />
                    <Text style={styles.title}>{book.book_name}</Text>

                    <View style={styles.content}>
                        <Text style={styles.text}>Author: {book.book_author}</Text>
                        <Text style={styles.text}>
                            Language: {book.book_language}
                        </Text>
                        <Text style={styles.text}>Status: {book.status}</Text>
                        <Text style={styles.text}>
                            Description: {book.book_describe}
                        </Text>
                    </View>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => setShow(true)}
                    >
                        <Text style={styles.textButton}>Reserve</Text>
                    </TouchableOpacity>

                    <Modal transparent={true} visible={show}>
                        <View style={styles.popup}>
                            <View style={styles.popupContent1}>
                                <Text style={styles.popupTitle}>
                                    When will you take this book?
                                </Text>
                                <View style={styles.popupInput}>
                                    <TextInput
                                        style={styles.popupBox}
                                        placeholder="dd"
                                        placeholderTextColor="grey"
                                        value={day || ""}
                                        onChangeText={(text) => setDay(text)}
                                        maxLength='2'
                                        keyboardType = 'numeric'
                                    />
                                    <TextInput
                                        style={styles.popupBox}
                                        placeholder="mm"
                                        placeholderTextColor="grey"
                                        value={month || ""}
                                        onChangeText={(text) => setMonth(text)}
                                        maxLength='2'
                                        keyboardType = 'numeric'
                                    />
                                    <TextInput
                                        style={styles.popupBox}
                                        placeholder="yy"
                                        placeholderTextColor="grey"
                                        value={year || ""}
                                        onChangeText={(text) => setYear(text)}
                                        maxLength='4'
                                        keyboardType = 'numeric'
                                    />
                                </View>
                                { alertD === true && <Text style={{ color: 'red', marginTop: 5, textAlign:'center'}}>Day invalid!</Text> }
                                { alertM === true && <Text style={{ color: 'red', marginTop: 5, textAlign:'center'}}>Month invalid!</Text> }
                                { alertY === true && <Text style={{ color: 'red', marginTop: 5, textAlign:'center'}}>Year invalid!</Text> }
                                <View
                                    style={{
                                        flexDirection: "row",
                                        justifyContent: "center",
                                    }}
                                >
                                    <TouchableOpacity
                                        style={styles.button}
                                        onPress={handleReverse}
                                    >
                                        <Text style={styles.textButton}>
                                            Reserve
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={styles.buttonCancel}
                                        onPress={() => setShow(false)}
                                    >
                                        <Text style={styles.textCancel}>
                                            Cancel
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>

                    <Modal transparent={true} visible={alert}>
                        <View style={styles.popup}>
                            <View style={styles.popupContent2}>
                                <Text style={{
                                        fontSize: 18,
                                        textAlign: "center",
                                        color: "green",
                                    }}
                                >
                                    Reserve Successfully!
                                </Text>
                                <View style={{ justifyContent: "center" }}>
                                    <TouchableOpacity
                                        style={styles.buttonCancel}
                                        onPress={() => setAlert(false)}
                                    >
                                        <Text style={styles.textCancel}>Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>

                    <Modal transparent={true} visible={alertF}>
                        <View style={styles.popup}>
                            <View style={styles.popupContent2}>
                                <Text
                                    style={{
                                        fontSize: 18,
                                        textAlign: "center",
                                        color: "red",
                                    }}
                                >
                                    {text}
                                </Text>
                                <View style={{ justifyContent: "center" }}>
                                    <TouchableOpacity
                                        style={styles.buttonCancel}
                                        onPress={() => setAlertF(false)}
                                    >
                                        <Text style={styles.textCancel}>Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        textAlign: 'center',
        fontSize: 24,
        padding: 10,
        marginBottom: 25,
    },
    image: {
        marginTop: 20,
        width: 200,
        height: 300,
        padding: 5,
        margin: 10,
        alignSelf: 'center'
    },
    text: {
        paddingLeft: 20,
        padding: 10,
        marginTop: 10,
        color: 'grey'
    },
    button: {
        marginTop: 20,
        alignSelf: 'center',
        width: 200,
        backgroundColor: '#cc8028',
        borderRadius: 10,
        padding: 10,
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 10,
    },
    buttonCancel: {
        marginTop: 20,
        alignSelf: 'center',
        width: 100,
        backgroundColor: '#59ACFF',
        borderRadius: 10,
        padding: 10,
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 10,
        marginLeft: 10,
    },
    textCancel: {
        color: 'white',
    },
    textButton: {
        color: 'white'
    },
    popup: {
        flex: 1,
        backgroundColor: '#000000aa',
    },
    popupContent1: {
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        marginHorizontal: 30,
        marginVertical: '70%'
    },
    popupContent2: {
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        marginHorizontal: 30,
        marginVertical: '80%'
    },
    popupTitle: {
        fontSize: 18,
        textAlign: 'center',
    },
    popupInput: {
        flexDirection: 'row',
        marginHorizontal: 10,
    },
    popupBox: {
        width: '30%',
        margin: 10,
        padding: 10,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 10,
    },
    header: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#3440FF',
        flex: 1,
    },
    imageBack: {
        width: 24,
        height: 24,
    }
});

export default BookDetail;