import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, CheckBox, ScrollView } from 'react-native';
import bookApi from '../../../api/BookService';

function Search({ navigation }) {

    const [book, setBook] = useState([]);
    const [input, setInput] = useState('');
    const [bookSearch, setBookSearch] = useState([]);
    const [bookSearch1, setBookSearch1] = useState([]);
    const [bookSearch2, setBookSearch2] = useState([]);
    const [name, setName] = useState(false);
    const [author, setAuthor] = useState(false);

    async function getBook() {
        const response = await bookApi.getAllBook();
        setBook(response.data)
    }

    function removeVietnameseTones(str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
        str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
        str = str.replace(/đ/g,"d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");
        // Some system encode vietnamese combining accent as individual utf-8 characters
        // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
        // Remove extra spaces
        // Bỏ các khoảng trắng liền nhau
        str = str.replace(/ + /g," ");
        str = str.trim();
        // Remove punctuations
        // Bỏ dấu câu, kí tự đặc biệt
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
        return str;
    }

    function handleSearch() {
        if (name === true) {
            if (input) {
                setBookSearch(book.filter((item) => removeVietnameseTones(item.book_name).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))));
            }
        }
        if (author === true) {
            if (input) {
                setBookSearch(book.filter((item) => removeVietnameseTones(item.book_author).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))));
            }
        }
        
        return bookSearch;   
    }

    const handleSearchAll = () => {
        if (input) {
            setBookSearch1(book.filter((item) => removeVietnameseTones(item.book_name).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))));
            setBookSearch2(book.filter((item) => removeVietnameseTones(item.book_author).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))));
        }
    }

    useEffect(() => {
        getBook();
    }, [name, author]); 

    return (
        <View style={styles.container}>
            <View style={styles.search}>
                <TextInput 
                    style={styles.input} 
                    placeholder='Search' 
                    value={input}
                    onChangeText={(text) => { setInput(text)}}
                />
                <Image style={styles.logo} src={require('../../../../assets/magnify.png')} />
                <TouchableOpacity style={styles.searchbtn} onPress={(name === false && author === false) ? handleSearchAll : handleSearch}><Text style={styles.searchbtn_text}>Search</Text></TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row', paddingTop: 10,justifyContent:'center', backgroundColor: 'white',paddingBottom: 20, borderBottomWidth: 1, borderBottomColor: 'grey',}}>
                <View style={{flexDirection: 'row', marginHorizontal: 20,}}>
                    <CheckBox
                        value={name}
                        onValueChange={setName}
                    />
                    <Text style={{ marginLeft: 10,}}>Name</Text> 
                </View>
                <View style={{flexDirection: 'row', marginHorizontal: 20,}}>
                    <CheckBox 
                        value={author}
                        onValueChange={setAuthor}
                    />
                    <Text style={{ marginLeft: 10,}}>Author</Text>
                </View>
            </View>

            <ScrollView>
            { name === false && author === false ? (
                <>
                    { bookSearch1.map((item, index) => (
                        <View key={index}>
                            <TouchableOpacity onPress={() => navigation.push('BookDetail', { id: item.id})}>
                                <View style={styles.content} >
                                    <View styles={styles.cover}>
                                        <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                    </View>
                                    <View style={styles.detail}>
                                        <View >
                                            <Text style={styles.name}>{item.book_name}</Text>
                                        </View>
                                        <Text style={styles.text1}>Author: {item.book_author}</Text>
                                        <Text style={styles.text1}>Language: {item.book_language}</Text>
                                        <Text style={styles.status}>{item.status}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ))}
                    { bookSearch2.map((item, index) => (
                        <View key={index}>
                            <TouchableOpacity onPress={() => navigation.push('BookDetail', { id: item.id})}>
                                <View style={styles.content} >
                                    <View styles={styles.cover}>
                                        <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                    </View>
                                    <View style={styles.detail}>
                                        <View >
                                            <Text style={styles.name}>{item.book_name}</Text>
                                        </View>
                                        <Text style={styles.text1}>Author: {item.book_author}</Text>
                                        <Text style={styles.text1}>Language: {item.book_language}</Text>
                                        <Text style={styles.status}>{item.status}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ))}
                </>
            ) :
            ( bookSearch.map((item, key) => (
                <View key={key}>
                    <TouchableOpacity onPress={() => navigation.navigate('BookDetail', { id: item.id})}>
                        <View style={styles.content} >
                            <View styles={styles.cover}>
                                <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                            </View>
                            <View style={styles.detail}>
                                <View >
                                    <Text style={styles.name}>{item.book_name}</Text>
                                </View>
                                <Text style={styles.text1}>Author: {item.book_author}</Text>
                                <Text style={styles.text1}>Language: {item.book_language}</Text>
                                <Text style={styles.status}>{item.status}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            )))}
            {}
            </ScrollView>

        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#efefef;',
    },
    title: {
        justifyContent: "center",
        fontSize: 24,
        textAlign: "center",
        backgroundColor: "#AAC9E8",
        padding: 15,
        fontWeight: 600,
    },
    search: {
        fleX: 1,
        backgroundColor: 'white',
        paddingBottom: 10,
    },
    input: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginLeft: 50,
        marginRight: 50,
        padding: 10,
        borderRadius: 15,
        marginTop: 20,
    },
    searchbtn: {
        padding: 5,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        width: 70,

    },  
    searchbtn_text: {
        textAlign: 'center',
    },
    logo: {
        width: '100%',
    },
    content: {
        flex: 1,
        marginTop: 10,
        flexDirection: 'row',
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: 'grey',
    },
    image: {
        width: 100,
        height: 150,
        padding: 5,
        margin: 10,
    },
    detail: {
        flex: 1,
        padding: 5,
    },
    name: {
        flex: 1,
        fontSize: 18,
        marginLeft: 10,
        marginBottom: 10,
    },
    text1: {
        marginLeft: 10,
        marginBottom: 2,
        fontSize: 12,
    },
    status: {
        color: 'green',
        fontSize: 14,
        marginLeft: '70%',
        marginTop: 30,
    }
});

export default Search;
