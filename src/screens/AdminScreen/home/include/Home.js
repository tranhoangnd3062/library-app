import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    TextInput,
    CheckBox,
    Modal
} from "react-native";
import bookApi from "../../../../api/BookService";

function Home({ navigation }) {
    const [book, setBook] = useState([]);
    const [input, setInput] = useState("");
    const [name, setName] = useState(false);
    const [author, setAuthor] = useState(false);
    const [refresh, setRefresh] = useState(true);
    const [alert1, setAlert1] = useState(false);
    const [alert2, setAlert2] = useState(false);

    async function handleDelete(id) {
        try {
            const response = await bookApi.deleteBookById(id);
            if (response.status == 200) {
                setRefresh(!refresh);
                setAlert1(true);
            } else {
                setAlert2(true);
            }
        } catch (e) { setAlert2(true); }
    }
    async function getBook() {
        const response = await bookApi.getAllBook();
        setBook(response.data);
    }

    function removeVietnameseTones(str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
        str = str.replace(/Đ/g, "D");
        // Some system encode vietnamese combining accent as individual utf-8 characters
        // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
        str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
        str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
        // Remove extra spaces
        // Bỏ các khoảng trắng liền nhau
        str = str.replace(/ + /g, " ");
        str = str.trim();
        // Remove punctuations
        // Bỏ dấu câu, kí tự đặc biệt
        str = str.replace(
            /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
            " "
        );
        return str;
    }

    useEffect(() => {
        getBook();
    }, [refresh]);

    return (
        <View style={styles.container}>
            <View style={styles.titlecontent}>
                <Text style={styles.title}>All Books</Text>
            </View>
            <ScrollView>
            <View style={styles.search}>
                <TextInput
                    style={styles.input}
                    placeholder="Search..."
                    value={input}
                    onChangeText={(text) => {
                        setInput(text);
                    }}
                />
                <Image
                    style={styles.logo}
                    source={require("../../../../../assets/magnify.png")}
                />
            </View>
            <View
                style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    marginTop: 20,
                    paddingBottom: 20,
                    borderBottomWidth: 1,
                    borderBottomColor: "grey",
                }}
            >
                <View
                    style={{ flexDirection: "row", marginHorizontal: 20 }}
                >
                    <CheckBox value={name} onValueChange={setName} />
                    <Text style={{ marginLeft: 10 }}>Name</Text>
                </View>
                <View
                    style={{ flexDirection: "row", marginHorizontal: 20 }}
                >
                    <CheckBox value={author} onValueChange={setAuthor} />
                    <Text style={{ marginLeft: 10 }}>Author</Text>
                </View>
            </View>
            <View>
                { input ? (
                    <>
                        { name === true && book.filter((item) => removeVietnameseTones(item.book_name).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))).map((item, index) => (
                            <View key={index}>
                                <TouchableOpacity onPress={() => navigation.push('BookDetailAdmin', { id: item.id})}>
                                    <View style={styles.content} >
                                        <View styles={styles.cover}>
                                            <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                        </View>
                                        <View style={styles.detail}>
                                            <View >
                                                <Text style={styles.name}>{item.book_name}</Text>
                                            </View>
                                            <Text style={styles.text1}>Author: {item.book_author}</Text>
                                            <Text style={styles.text1}>Language: {item.book_language}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'column', marginTop: 40}}>
                                            <TouchableOpacity 
                                                style={{ backgroundColor: '#E57878',
                                                        borderRadius: 5, marginBottom: 10, 
                                                        padding: 5, width: 60, marginRight: 5
                                                    }}
                                                onPress={() => navigation.push('BookEdit' , {id: item.id})}
                                            >
                                                <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500 }}>Edit</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity  
                                                style={{ backgroundColor: '#E57878', 
                                                    borderRadius: 5, marginBottom: 10, 
                                                    padding: 5, width: 60, marginRight: 5
                                                }}
                                                onPress={() => handleDelete(item.id)}
                                            >
                                                <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500  }}>Delete</Text>
                                            </TouchableOpacity>
                                            <Text style={styles.status}>{item.status}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        ))}
                        { author === true && book.filter((item) => removeVietnameseTones(item.book_author).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))).map((item, index) => (
                            <View key={index}>
                                <TouchableOpacity onPress={() => navigation.push('BookDetailAdmin', { id: item.id})}>
                                    <View style={styles.content} >
                                        <View styles={styles.cover}>
                                            <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                        </View>
                                        <View style={styles.detail}>
                                            <View >
                                                <Text style={styles.name}>{item.book_name}</Text>
                                            </View>
                                            <Text style={styles.text1}>Author: {item.book_author}</Text>
                                            <Text style={styles.text1}>Language: {item.book_language}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'column', marginTop: 40}}>
                                            <TouchableOpacity 
                                                style={{ backgroundColor: '#E57878',
                                                        borderRadius: 5, marginBottom: 10, 
                                                        padding: 5, width: 60, marginRight: 5
                                                    }}
                                                onPress={() => navigation.push('BookEdit', {id: item.id})}
                                            >
                                                <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500 }}>Edit</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity  
                                                style={{ backgroundColor: '#E57878', 
                                                    borderRadius: 5, marginBottom: 10, 
                                                    padding: 5, width: 60, marginRight: 5
                                                }}
                                                onPress={() => handleDelete(item.id)}
                                            >
                                                <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500  }}>Delete</Text>
                                            </TouchableOpacity>
                                            <Text style={styles.status}>{item.status}</Text>
                                        </View>
                                    </View>  
                                </TouchableOpacity>
                            </View>
                        ))}
                        { name === false && author === false && 
                            <>
                                { book.filter((item) => removeVietnameseTones(item.book_name).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))).map((item, index) => (
                                    <View key={index}>
                                        <TouchableOpacity onPress={() => navigation.push('BookDetailAdmin', { id: item.id})}>
                                            <View style={styles.content} >
                                                <View styles={styles.cover}>
                                                    <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                                </View>
                                                <View style={styles.detail}>
                                                    <View >
                                                        <Text style={styles.name}>{item.book_name}</Text>
                                                    </View>
                                                    <Text style={styles.text1}>Author: {item.book_author}</Text>
                                                    <Text style={styles.text1}>Language: {item.book_language}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'column', marginTop: 40}}>
                                                    <TouchableOpacity 
                                                        style={{ backgroundColor: '#E57878',
                                                                borderRadius: 5, marginBottom: 10, 
                                                                padding: 5, width: 60, marginRight: 5
                                                            }}
                                                        onPress={() => navigation.push('BookEdit', {id: item.id})}
                                                    >
                                                        <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500 }}>Edit</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity  
                                                        style={{ backgroundColor: '#E57878', 
                                                            borderRadius: 5, marginBottom: 10, 
                                                            padding: 5, width: 60, marginRight: 5
                                                        }}
                                                        onPress={() => handleDelete(item.id)}
                                                    >
                                                        <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500  }}>Delete</Text>
                                                    </TouchableOpacity>
                                                    <Text style={styles.status}>{item.status}</Text>
                                                </View>
                                            </View>
                                            
                                        </TouchableOpacity>
                                    </View>
                                ))}
                                { book.filter((item) => removeVietnameseTones(item.book_author).toLowerCase().match(removeVietnameseTones(input.toLowerCase()))).map((item, index) => (
                                    <View key={index}>
                                        <TouchableOpacity onPress={() => navigation.push('BookDetailAdmin', { id: item.id})}>
                                            <View style={styles.content} >
                                                <View styles={styles.cover}>
                                                    <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                                </View>
                                                <View style={styles.detail}>
                                                    <View >
                                                        <Text style={styles.name}>{item.book_name}</Text>
                                                    </View>
                                                    <Text style={styles.text1}>Author: {item.book_author}</Text>
                                                    <Text style={styles.text1}>Language: {item.book_language}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'column', marginTop: 40}}>
                                                    <TouchableOpacity 
                                                        style={{ backgroundColor: '#E57878',
                                                                borderRadius: 5, marginBottom: 10, 
                                                                padding: 5, width: 60, marginRight: 5
                                                            }}
                                                        onPress={() => navigation.push('BookEdit',{id: item.id})}
                                                    >
                                                        <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500 }}>Edit</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity  
                                                        style={{ backgroundColor: '#E57878', 
                                                            borderRadius: 5, marginBottom: 10, 
                                                            padding: 5, width: 60, marginRight: 5
                                                        }}
                                                        onPress={() => handleDelete(item.id)}
                                                    >
                                                        <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500  }}>Delete</Text>
                                                    </TouchableOpacity>
                                                    <Text style={styles.status}>{item.status}</Text>
                                                </View>
                                            </View>
                                            
                                        </TouchableOpacity>
                                    </View>
                                ))}
                            </>
                        }
                    </>
                ) : 
                (
                    book.map((item, key) => (
                        <View key={key}>
                            <TouchableOpacity onPress={() => navigation.push('BookDetailAdmin', { id: item.id})}>
                                <View style={styles.content} >
                                    <View styles={styles.cover}>
                                        <Image style={styles.image} source={{ uri: `${item.book_image}` }} />
                                    </View>
                                    <View style={styles.detail}>
                                        <View >
                                            <Text style={styles.name}>{item.book_name}</Text>
                                        </View>
                                        <Text style={styles.text1}>Author: {item.book_author}</Text>
                                        <Text style={styles.text1}>Language: {item.book_language}</Text>
                                        
                                    </View>
                                    <View style={{ flexDirection: 'column', marginTop: 40}}>
                                        <TouchableOpacity 
                                            style={{ backgroundColor: '#E57878',
                                                    borderRadius: 5, marginBottom: 10, 
                                                    padding: 5, width: 60, marginRight: 5
                                                }}
                                            onPress={() => navigation.push('BookEdit', {id: item.id})}
                                        >
                                            <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500 }}>Edit</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  
                                            style={{ backgroundColor: '#E57878', 
                                                borderRadius: 5, marginBottom: 10, 
                                                padding: 5, width: 60, marginRight: 5
                                            }}
                                            onPress={() => handleDelete(item.id)}
                                        >
                                            <Text style={{ textAlign: 'center', color: 'white', fontWeight: 500  }}>Delete</Text>
                                        </TouchableOpacity>
                                        <Text style={styles.status}>{item.status}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ))
                )}
                <Modal transparent={true} visible={alert1}>
                    <View style={styles.popup}>
                        <View style={styles.popupContent2}>
                            <Text style={{
                                    fontSize: 18,
                                    textAlign: "center",
                                    color: "green",
                                }}
                            >
                                Delete Successfully!
                            </Text>
                            <View style={{ justifyContent: "center" }}>
                                <TouchableOpacity
                                    style={styles.buttonCancel}
                                    onPress={() => setAlert1(false)}
                                >
                                    <Text style={styles.textCancel}>Close</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                <Modal transparent={true} visible={alert2}>
                    <View style={styles.popup}>
                        <View style={styles.popupContent2}>
                            <Text style={{
                                    fontSize: 18,
                                    textAlign: "center",
                                    color: "red",
                                }}
                            >
                                Delete Unsuccessfully!
                            </Text>
                            <View style={{ justifyContent: "center" }}>
                                <TouchableOpacity
                                    style={styles.buttonCancel}
                                    onPress={() => setAlert2(false)}
                                >
                                    <Text style={styles.textCancel}>Close</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    title: {
        justifyContent: "center",
        fontSize: 24,
        textAlign: "center",
        backgroundColor: "#AAC9E8",
        padding: 15,
        fontWeight: 600,
    },
    filter: {
        flexDirection: "row",
        marginBottom: 5,
        marginTop: 30,
        justifyContent: "center",
    },
    checkbox: {
        marginLeft: 30,
    },
    label: {
        marginLeft: 10,
        marginRight: 20,
    },
    content: {
        flex: 1,
        marginTop: 30,
        flexDirection: "row",
        width: "100%",
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        backgroundColor: "white",
        paddingBottom: 20,
    },
    image: {
        width: 100,
        height: 150,
        padding: 5,
        margin: 10,
    },
    detail: {
        flex: 1,
        padding: 5,
    },
    name: {
        flex: 1,
        fontSize: 18,
        marginLeft: 10,
        marginBottom: 10,
    },
    text1: {
        marginLeft: 10,
        marginBottom: 2,
        fontSize: 12,
    },
    search: {
        flex: 1,
        backgroundColor: "white",
        // marginBottom: 15,
        flexDirection: "row",
        justifyContent: "center",
        width: "100%",
    },
    input: {
        borderWidth: 1,
        borderColor: "black",
        padding: 10,
        borderRadius: 15,
        marginTop: 20,
        width: "80%",
    },
    searchbtn: {
        padding: 5,
        alignSelf: "center",
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 5,
        width: 70,
    },
    searchbtn_text: {
        textAlign: "center",
    },
    logo: {
        width: 20,
        height: 20,
        top: "50%",
        right: "9%",
    },
    status: {
        color: "green",
        fontSize: 14,
        marginTop: 30,
    },
    buttonREST: {
        textAlign: "center",
        top: -55,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "dark",
        padding: 7,
        minWidth: 50,
        maxWidth: 100,
    },
    popup: {
        flex: 1,
        backgroundColor: '#000000aa',
    },
    popupContent2: {
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        marginHorizontal: 30,
        marginVertical: '80%'
    },
    textCancel: {
        color: 'white',
    },
    buttonCancel: {
        marginTop: 20,
        alignSelf: 'center',
        width: 100,
        backgroundColor: '#59ACFF',
        borderRadius: 10,
        padding: 10,
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 10,
        marginLeft: 10,
    },
});

export default Home;
