import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import authApi from '../../../../api/AuthService';
import React, {useState, useEffect} from 'react';

function UserInfo({ navigation }) {

    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [doB, setDoB] = useState();
    const [sex, setSex] = useState();
    const [email, setEmail] = useState();
    const [address, setAddress] = useState();
    const [phone, setPhone] = useState();

    const getUserInfo = async () => {
        const response = await authApi.getUser();
        if (response.status === 200) {
            setFirstName(response.data.firstname);
            setLastName(response.data.lastname);
            setDoB(response.data.date_of_birth.slice(0,11));
            setSex(response.data.sex);
            setEmail(response.data.email);
            setPhone(response.data.phone_number);
            setAddress(response.data.address);

        }
    }

    useEffect(() => {
        getUserInfo();
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{width: '50%'}}>
                    <TouchableOpacity  onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: 'row', marginLeft: 5}}>
                        <Image style={styles.imageBack} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png' }}/>
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                    </TouchableOpacity>
                </View>
                <View style={{width: '50%'}} >
                    <TouchableOpacity onPress={() => navigation.push('EditInfo')}>
                        <Text style={{ fontSize: 17, textAlign: 'right', marginRight: 10, color: 'white'}}>Chỉnh sửa</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
            
            <View style={styles.user}>
                <View style={styles.info}>
                    <Image style={styles.avatar} source={require('../../../../../assets/user.png')} />
                    <Text style={styles.name}>{firstName + ' ' + lastName}</Text>
                </View>

                <View style={styles.list}>
                    <TouchableOpacity style={styles.tablist}>
                        <Text style={styles.text1}>Full Name</Text>
                        <Text style={styles.text2}>{firstName + ' ' + lastName}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tablist}>
                        <Text style={styles.text1}>Date of Birth</Text>
                        <Text style={styles.text2}>{doB}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tablist}>
                        <Text style={styles.text1}>Sex</Text>
                        <Text style={styles.text2}>{sex}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tablist}>
                        <Text style={styles.text1}>Phone</Text>
                        <Text style={styles.text2}>{phone}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tablist}>
                        <Text style={styles.text1}>Email</Text>
                        <Text style={styles.text2}>{email}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tablist}>
                        <Text style={styles.text1}>Address</Text>
                        <Text style={styles.text2}>{address}</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#efefef',
    },
    user: {
        width: '100%',
    },
    avatar: {
        marginTop: 20,
        width: 100,
        height: 100,
        alignSelf: 'center',
        marginBottom: 10,
        backgroundColor: 'white',
        borderRadius: 50,
        borderColor: '#BFBFBF',
        borderWidth: 3,
    },
    info: {
        backgroundColor: '#FFA164',
    },
    name: {
        width: '100%',
        fontSize: 24,
        alignSelf: 'center',
        textAlign: 'center',
        backgroundColor: 'white',
        paddingBottom: 20,
    },
    list: {
        marginTop: 10,
        flexDirection: 'column'
    },
    tablist: {
        padding: 10,
        backgroundColor: 'white',
        marginBottom: 8,
    },
    text1: {
        fontSize: 18,
    },
    text2: {
        fontSize: 14,
        color: 'grey'
    },
    header: {
        textAlign: 'center',
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#3440FF',
    },
    title: {
        textAlign: 'center',
        width: '60%',
        fontSize: 17,
    },
    back: {
        width: '20%',
        textAlign: 'left',
        marginLeft: 10,
    },
    button: {
        width: '50%',
        textAlign: 'right',
        marginRight: 10,
    },
    imageBack: {
        width: 24,
        height: 24,
    }
});

export default UserInfo;