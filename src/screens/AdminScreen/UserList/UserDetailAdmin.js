import { useEffect, useState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    Image,
} from "react-native";
import userApi from "../../../api/UserService";

function userDetailAdmin({ navigation, route }) {
    let { id } = route.params;
    const [user, setUser] = useState([]);

    async function getUserById() {
        const response = await userApi.getUserById(id);
        setUser(response.data);
    }

    useEffect(() => {
        getUserById();
    }, [id]);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: "row", marginLeft: 5 }}>
                        <Image
                            style={styles.imageBack}
                            source={{
                                uri: "https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png",
                            }}
                        />
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <ScrollView>
                <View style={styles.content}>
                    <View style={styles.detail}>
                        <Text style={styles.text1}>
                            Username: {user.user_name}
                        </Text>
                        <Text style={styles.text1}>
                            Firstname: {user.firstname}
                        </Text>
                        <Text style={styles.text1}>
                            Lastname: {user.lastname}
                        </Text>
                        <Text style={styles.text1}>
                            Date of birth: {user.day + '-' + user.month + '-' + user.year}
                        </Text>
                        <Text style={styles.text1}>
                            Email: {user.email}
                        </Text>
                        <Text style={styles.text1}>
                            Phone: {user.phone_number}
                        </Text>
                        <Text style={styles.text1}>
                            Address: {user.address}
                        </Text>
                        <Text style={styles.text1}>
                            Sex: {user.sex}
                        </Text>
                        <Text style={styles.text1}>Status: {user.status}</Text>
                        <TouchableOpacity
                            styles={{ flex: 1 }}
                            onPress={() => {
                                handleDelete(user.id);
                            }}
                        ></TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },

    title: {
        justifyContent: "center",
        fontSize: 24,
        textAlign: "center",
        backgroundColor: "#AAC9E8",
        padding: 15,
        fontWeight: 600,
    },
    filter: {
        flexDirection: "row",
        marginBottom: 5,
        marginTop: 30,
        justifyContent: "center",
    },
    checkbox: {
        marginLeft: 30,
    },
    label: {
        marginLeft: 10,
        marginRight: 20,
    },
    content: {
        flex: 1,
        marginTop: 30,
        flexDirection: "row",
        width: "100%",
        backgroundColor: "white",
        paddingBottom: 20,
    },
    image: {
        width: 100,
        height: 150,
        padding: 5,
        margin: 10,
    },
    detail: {
        flex: 1,
        padding: 5,
    },
    name: {
        flex: 1,
        fontSize: 18,
        marginLeft: 10,
        marginBottom: 10,
    },
    text1: {
        marginLeft: 10,
        marginBottom: 2,
        fontSize: 20,
    },
    status: {
        color: "green",
        fontSize: 14,
        marginLeft: "70%",
        marginTop: 30,
    },
    search: {
        fleX: 1,
        backgroundColor: "white",
        // marginBottom: 15,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        flexDirection: "row",
        justifyContent: "center",
    },
    input: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: "black",
        padding: 10,
        borderRadius: 15,
        marginTop: 20,
    },
    searchbtn: {
        padding: 5,
        alignSelf: "center",
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 5,
        width: 70,
    },
    searchbtn_text: {
        textAlign: "center",
    },
    logo: {
        width: 20,
        height: 20,
        top: "41%",
        right: "9%",
    },
    status: {
        color: "green",
        fontSize: 14,
        marginLeft: "70%",
        marginTop: 30,
    },
    buttonREST: {
        textAlign: "center",
        top: -55,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "dark",
        padding: 7,
        minWidth: 50,
        maxWidth: 100,
    },
    header: {
        width: "100%",
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: "#3440FF",
    },
    imageBack: {
        width: 24,
        height: 24,
    },
});

export default userDetailAdmin;
