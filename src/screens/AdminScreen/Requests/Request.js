import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, CheckBox } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import bookApi from '../../../api/BookService';

function Request({ navigation }) {

    const [borrow, setBorrow] = useState(false);
    const [lend, setLend] = useState(false);
    const [borrowDay, setBorrowDay] = useState();
    const [show, setShow] = useState(false);
    const [alert1, setAlert1] = useState(false);
    const [alert2, setAlert2] = useState(false);
    const [idReserve, setIdReserve] = useState();

    const requestBorrow = async () => {
        try {

                let data = { id_promissory_note: idReserve, borrow_days: borrowDay }
                const response = await bookApi.requestBorrow(data)
                if (response.status == 201) {
                    setShow(true);
                    setAlert1(true);
                    setShow(false);
                    conssole.log('ok')
                }  
            
        } catch {
            setAlert2(true);
            console.log('Check k thanh cong')
        }
    }

    const requestLend = async () => {
        try {

                let data = { id_log_book: idReserve }
                const response = await bookApi.requestLend(data)
                if (response.status == 201) {
                    setShow(true);
                    setAlert1(true);
                    setShow(false);
                    conssole.log('ok')
                }  
            
        } catch {
            setAlert2(true);
            console.log('Check k thanh cong')
        }
    }

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.titlecontent}>
                    <Text style={styles.title}>Request</Text>
                </View>
                <View style={{marginTop: 50 }}>
                    <Text style={{ marginBottom: 10, marginLeft: 20, fontSize: 20, }}>ID Reserve: </Text>
                    <TextInput 
                            style={{ borderWidth: 1, 
                                    borderColor: 'grey', 
                                    padding: 15, 
                                    borderRadius: 10, 
                                    marginHorizontal: 20,
                                }} 
                            placeholder='ID...'
                            value={idReserve || ''}
                            onChangeText={(text) => setIdReserve(text)}
                    />
                    { borrow === true &&
                        <>
                            <Text style={{ marginBottom: 10, marginLeft: 20, fontSize: 20, marginTop: 10,}}>Number of days to borrow: </Text>
                            <TextInput
                                    value={borrowDay || ''}
                                    onChangeText={(text) => setBorrowDay(text)}
                                    style={{ borderWidth: 1, 
                                            borderColor: 'grey', 
                                            padding: 15, 
                                            borderRadius: 10, 
                                            marginHorizontal: 20,
                                        }} 
                                    placeholder='Enter 1-10'
                            /> 
                        </>
                    }
                    
                    <View style={{flexDirection: 'row', justifyContent:'center', marginTop: 20,}}>
                        <View style={{flexDirection: 'row', marginHorizontal: 20,}}>
                           <CheckBox
                                value={borrow || ''}
                                onValueChange={setBorrow}
                           />
                            <Text style={{ marginLeft: 10,}}>Borrow</Text> 
                        </View>
                        <View style={{flexDirection: 'row', marginHorizontal: 20,}}>
                            <CheckBox 
                                value={lend || ''}
                                onValueChange={setLend}
                            />
                            <Text style={{ marginLeft: 10,}}>Lend</Text>
                        </View>
                    </View>
                </View>
                <TouchableOpacity 
                    style={{
                        textAlign: 'center',
                        backgroundColor: '#007EFF',
                        padding: 10,
                        width: '60%',
                        alignSelf: 'center',
                        borderRadius: 10,
                        marginTop: 20,
                    }}
                    onPress={() => borrow === true ? requestBorrow() : (lend === true ? requestLend() : setShow(true))}
                >
                    <Text style={{color: 'white'}}>Confirm</Text>
                </TouchableOpacity> 
                { alert1 === true && <Text style={{ color: 'green', textAlign: 'center', marginTop: 15}}>Success!</Text> }
                { alert2 === true && <Text style={{ color: 'red', textAlign: 'center', marginTop: 15}}>Failed!</Text> }
                { show === true && <Text style={{ color: 'red', textAlign: 'center', marginTop: 15}}>Please choose Borrow or Lend!</Text> }
            </ScrollView>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    title: {
        justifyContent: 'center',
        fontSize: 24,
        textAlign: 'center',
        backgroundColor: '#AAC9E8',
        padding: 15,
        fontWeight: 600,
    },

});

export default Request;
