import { View, ImageBackground, Text, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';
import React, {useState} from 'react';
import authApi from '../../api/AuthService';

const image = { uri: require('../../../assets/main.jpg') }

function ForgotPasswordChange({navigation}) {

    const [newPassword, setNewPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [alert, setAlert] = useState(false);

    const handleChangePassword = async () => {
        try {
            let data = { new_password: newPassword, confirm_new_password: confirmPassword };
            const response = await authApi.forgetPasswordChange(data);
            if (response.status === 200)
            {
                console.log('Doi thanh cong')
                // navigation.navigate('HomeScreen');
            }
        } catch {
            console.log('Doi mat khau that bai');
            setAlert(true);
        }
        
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: 'row', marginLeft: 5, backgroundColor: '#EF9A53',}}>
                        <Image style={styles.imageBack} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png' }}/>
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500, }}>Back</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <ImageBackground style={styles.logo} resizeMode="cover" source={image}>
                <Text style={styles.title}>Change Password</Text>
                
                {/* them check new === confirm */}
                <Text style={styles.titleInput}>New password</Text>
                <TextInput style={styles.input} secureTextEntry={true} value={newPassword} onChangeText={(text)=>setNewPassword(text)} placeholder="New Pasword " />
                <Text style={styles.titleInput}>Confirm new password</Text>
                <TextInput style={styles.input} secureTextEntry={true} value={confirmPassword} onChangeText={(text)=>setConfirmPassword(text)} placeholder="Confirm New Password" />

                { alert === true && <Text style={styles.text1}>Doi that bai</Text>}
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.text_button} onPress={handleChangePassword}> Confirm</Text>
                </TouchableOpacity>
                
            </ImageBackground>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        flex: 1,
    },
    image: {
        width: "100%"
    },
    title: {
        color: "#F2DEBA",
        fontWeight: 'bold',
        marginTop: 190,
        fontSize: 30,
        marginBottom: 100,
        textAlign: 'center',
    },
    titleInput: {
        color: "#F2DEBA",
        fontWeight: 700,
        fontSize: 16,
        marginLeft: 55,
    },
    input: {
        height: 40,
        width: '70%',
        borderWidth: 1,
        padding: 10,
        margin: 10,
        borderRadius: 5,
        marginLeft: "15%",
        backgroundColor: '#FFEFD6'
    },
    button: {
        backgroundColor: '#EF9A53',
        width: '70%',
        padding: 8,
        marginTop: 30,
        borderRadius: 5,
        marginLeft: "15%",
        marginBottom: 90,
    },
    text_button: {
        color: 'white',
        fontWeight: 500,
        textAlign: 'center',
    },
    text1: {
        color: '#FDFDBD',
        textAlign: 'center',
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#3440FF',

    },
    imageBack: {
        width: 24,
        height: 24,
    }
});

export default ForgotPasswordChange;