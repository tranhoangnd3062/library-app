import { View, ImageBackground, Text, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';
import React, {useState} from 'react';
import authApi from '../../api/AuthService';

const image = { uri: require('../../../assets/main.jpg') }

function ForgotPassword({ navigation }) {

    const [username, setUsername] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [alert, setAlert] = useState();

    const forgetPassword = async () => {
        try {
            let data = { email: email, phone_number: phone, user_name: username }
            const response = await authApi.forgetPassword(data);
            if(response.status === 200) {
                console.log('Xac thuc thanh cong')
                localStorage.setItem("user", JSON.stringify(response.data));
                const token = localStorage.getItem("user");
                const tokenString = JSON.parse(token);
                localStorage.setItem("accessToken", tokenString.token);
                navigation.push('ForgotPasswordChange')
            }
        } catch {
            setAlert(true);
        }
        
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: 'row', marginLeft: 5,backgroundColor: '#EF9A53',}}>
                        <Image style={styles.imageBack} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png' }}/>
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>Back</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <ImageBackground style={styles.logo} resizeMode="cover" source={image}>
                <Text style={styles.title}>Forgot Password</Text>

                <Text style={styles.titleInput}>Email</Text>
                <TextInput style={styles.input} value={email || ''} onChangeText={(text)=>setEmail(text)} placeholder="Email " />
                <Text style={styles.titleInput}>Phone Number</Text>
                <TextInput style={styles.input} value={phone || ''} onChangeText={(text)=>setPhone(text)} placeholder="Phone Number" />
                <Text style={styles.titleInput}>Username</Text>
                <TextInput style={styles.input} value={username || ''} onChangeText={(text)=>setUsername(text)} placeholder="Username" />

                { alert === true && <Text style={styles.text1}>Xac thuc that bai</Text>}
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.text_button} onPress={forgetPassword}> Next</Text>
                </TouchableOpacity>
                
            </ImageBackground>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        flex: 1,
    },
    image: {
        width: "100%"
    },
    title: {
        color: "#F2DEBA",
        fontWeight: 'bold',
        marginTop: 150,
        fontSize: 30,
        marginBottom: 60,
        textAlign: 'center',
    },
    titleInput: {
        color: "#F2DEBA",
        fontWeight: 700,
        fontSize: 16,
        marginLeft: 55,
    },
    input: {
        height: 40,
        width: '70%',
        borderWidth: 1,
        padding: 10,
        margin: 10,
        borderRadius: 5,
        marginLeft: "15%",
        backgroundColor: '#FFEFD6'
    },
    button: {
        backgroundColor: '#EF9A53',
        width: '70%',
        padding: 8,
        marginTop: 30,
        borderRadius: 5,
        marginLeft: "15%",
        marginBottom: 90,
    },
    text_button: {
        color: 'white',
        fontWeight: 500,
        textAlign: 'center',
    },
    text1: {
        textAlign: 'center',
        color: '#FDFDBD'
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#3440FF',
    },
    imageBack: {
        width: 24,
        height: 24,
    }
});

export default ForgotPassword;