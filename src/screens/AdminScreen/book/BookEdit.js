import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    Modal,
    TextInput,
    Alert,
} from "react-native";
import bookApi from "../../../api/BookService";

function BookEdit({ route, navigation }) {
    let { id } = route.params;
    console.log(id)
    const [book_name, setBook_name] = useState();
    const [book_type, setBook_type] = useState();
    const [book_describe, setBook_describe] = useState();
    const [book_author, setBook_author] = useState();
    const [book_publisher, setBook_publisher] = useState();
    const [book_cost, setBook_cost] = useState();
    const [book_image, setBook_image] = useState();
    const [book_language, setBook_language] = useState();

    async function updateBook() {
        try {
            let data = {
                id: id,
                book_name: book_name,
                book_type: book_type,
                book_describe: book_describe,
                book_author: book_author,
                book_publisher: book_publisher,
                book_cost: book_cost,
                book_image: book_image,
                book_language: book_language,
            };

            const response = await bookApi.updateBook(data);

            if (response.status == 200) {
                Alert.alert("Thông báo", "Thay đổi thành công");
                setShow(true);
            } else {
                Alert.alert("Thông báo", "Thay đổi thất bại");
            }
        } catch {
            if (!true) {
            } else {
            }
        }
    }

    const [show, setShow] = useState(false);

    async function getBookById() {
        const response = await bookApi.getBookById(id);
        setBook_name(response.data.book_name);
        setBook_type(response.data.book_type);
        setBook_describe(response.data.book_describe);
        setBook_author(response.data.book_author);
        setBook_publisher(response.data.book_publisher);
        setBook_cost(response.data.book_cost);
        setBook_image(response.data.book_image);
        setBook_language(response.data.book_language);
    }

    useEffect(() => {
        getBookById();
    }, [id]);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <View style={{ flexDirection: "row", marginLeft: 5 }}>
                        <Image
                            style={styles.imageBack}
                            source={{
                                uri: "https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png",
                            }}
                        />
                        <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View>
                <View style={styles.content}>
                    <Image style={styles.image} source={{ uri: book_image }} />
                    <TextInput
                        style={[styles.input, { fontSize: 20 }]}
                        value={book_name || ""}
                        onChangeText={(text) => setBook_name(text)}
                        placeholderTextColor="grey"
                        placeholder="book_name"
                        numberOfLines={10}
                    />
                </View>
                {/* Book Type */}
                <View style={styles.content}>
                    <Text style={styles.titleInput}>Type</Text>
                    <TextInput
                        style={styles.input}
                        value={book_type || ""}
                        onChangeText={(text) => setBook_type(text)}
                        placeholderTextColor="grey"
                        placeholder="book_type"
                    />
                </View>
                {/* Describe */}
                <View style={styles.content}>
                    <Text style={styles.titleInput}>Describe</Text>
                    <TextInput
                        style={styles.input}
                        value={book_describe || ""}
                        onChangeText={(text) => setBook_describe(text)}
                        placeholderTextColor="grey"
                        placeholder="book_describepe"
                    />
                </View>

                {/* Author */}
                <View style={styles.content}>
                    <Text style={styles.titleInput}>Author</Text>
                    <TextInput
                        style={styles.input}
                        value={book_author || ""}
                        onChangeText={(text) => setBook_author(text)}
                        placeholderTextColor="grey"
                        placeholder="book_author"
                    />
                </View>
                {/* Publishere */}
                <View style={styles.content}>
                    <Text style={styles.titleInput}>Publishere</Text>
                    <TextInput
                        style={styles.input}
                        value={book_publisher || ""}
                        onChangeText={(text) => setBook_publisher(text)}
                        placeholderTextColor="grey"
                        placeholder="Book_publisher"
                    />
                </View>
                {/* Cost */}
                <View style={styles.content}>
                    <Text style={styles.titleInput}>Cost</Text>
                    <TextInput
                        style={styles.input}
                        value={book_cost || ""}
                        onChangeText={(text) => setBook_cost(text)}
                        placeholderTextColor="grey"
                        placeholder="book_cost"
                    />
                </View>
                {/* Image link*/}
                <View style={styles.content}>
                    <Text style={styles.titleInput}>Image link</Text>
                    <TextInput
                        style={styles.input}
                        value={book_image || ""}
                        onChangeText={(text) => setBook_image(text)}
                        placeholderTextColor="grey"
                        placeholder="book_image"
                    />
                </View>
                {/* Language*/}
                <View style={styles.content}>
                    <Text style={styles.titleInput}>Language</Text>
                    <TextInput
                        style={styles.input}
                        value={book_language || ""}
                        onChangeText={(text) => setBook_language(text)}
                        placeholderTextColor="grey"
                        placeholder="book_language"
                    />
                </View>

                <TouchableOpacity style={styles.button} onPress={updateBook}>
                    <Text style={styles.textButton}>Update</Text>
                </TouchableOpacity>
                <Modal transparent={true} visible={show}>
                    <View style={styles.popup}>
                        <View style={styles.popupContent}>
                            <Text style={styles.popupTitle}>
                                Thay đổi thành công !
                            </Text>
                            <View
                                style={{
                                    flexDirection: "row",
                                    justifyContent: "center",
                                }}
                            >
                                <TouchableOpacity
                                    style={styles.buttonCancel}
                                    onPress={() => setShow(false)}
                                >
                                    <Text style={styles.textCancel}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    title: {
        textAlign: "center",
        fontSize: 24,
        padding: 10,
        marginBottom: 10,
    },
    content: {
        backgroundColor: "white",
        padding: 10,
        paddingTop: 20,
    },
    image: {
        marginTop: 20,
        width: 200,
        height: 300,
        padding: 5,
        margin: 10,
        alignSelf: "center",
    },
    text: {
        paddingLeft: 20,
        padding: 10,
        marginTop: 10,
        color: "grey",
    },
    titleInput: {
        marginLeft: 10,
        marginBottom: 5,
    },
    input: {
        padding: 10,
        marginHorizontal: 10,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: "#D3D3D3",
        borderRadius: 8,
    },
    button: {
        marginTop: 20,
        alignSelf: "center",
        width: 200,
        backgroundColor: "#cc8028",
        borderRadius: 10,
        padding: 10,
        textAlign: "center",
        fontSize: 18,
        marginBottom: 10,
    },
    buttonCancel: {
        marginTop: 20,
        alignSelf: "center",
        width: 200,
        backgroundColor: "#59ACFF",
        borderRadius: 10,
        padding: 10,
        textAlign: "center",
        fontSize: 18,
        marginBottom: 10,
        marginLeft: 10,
    },
    textCancel: {
        color: "black",
    },
    textButton: {
        color: "white",
    },
    popup: {
        flex: 1,
        backgroundColor: "#000000aa",
    },
    popupContent: {
        justifyContent: "center",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 10,
        marginHorizontal: 30,
        marginVertical: "70%",
    },
    popupTitle: {
        fontSize: 18,
        textAlign: "center",
    },
    popupInput: {
        flexDirection: "row",
        marginHorizontal: 10,
    },
    popupBox: {
        width: "30%",
        margin: 10,
        padding: 10,
        borderWidth: 1,
        borderColor: "grey",
        borderRadius: 10,
    },
    header: {
        flexDirection: "row",
        width: "100%",
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: "#3440FF",
        flex: 1,
    },
    imageBack: {
        width: 24,
        height: 24,
    },
});

export default BookEdit;
