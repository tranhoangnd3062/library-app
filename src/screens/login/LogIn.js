import {
    View,
    ImageBackground,
    Text,
    TouchableOpacity,
    TextInput,
    StyleSheet,
    Image,
} from "react-native";
import React, { useState } from "react";
import authApi from "../../api/AuthService";

const image = { uri: require("../../../assets/main.jpg") };

function LogIn({ navigation }) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [usernameError, setUsernameError] = useState();
    const [passwordError, setPasswordError] = useState();
    const [error, setError] = useState();

    async function handleLogin() {
        setError("");
        setUsernameError("");
        setPasswordError("");
        if (!username && !password) {
            setUsernameError("Username cannot be empty!");
            setPasswordError("Password cannot be empty!");
        } else if (!password) {
            setPasswordError("Password cannot be empty!");
        } else if (!username) {
            setUsernameError("Username cannot be empty!");
        } else {
            try {
                let data = { user_name: username, user_password: password };
                const response = await authApi.login(data);

                if (response.status == 200) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                    const token = localStorage.getItem("user");
                    const tokenString = JSON.parse(token);
                    localStorage.setItem("accessToken", tokenString.token);
                    window.location.reload();
                }
            } catch {
                setError("Wrong username or password! Try again.");
            }
        }
    }

    return (
        <View style={styles.container}>
            <ImageBackground
                style={styles.logo}
                resizeMode="cover"
                source={image}
            >
                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("Home")}
                    >
                        <View
                            style={{
                                flexDirection: "row",
                                marginLeft: 5,
                                backgroundColor: "#EF9A53",
                            }}
                        >
                            <Image
                                style={styles.imageBack}
                                source={{
                                    uri: "https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png",
                                }}
                            />
                            <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>
                                Back
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={styles.title}>Welcome back.</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Username"
                    value={username}
                    onChangeText={(text) => setUsername(text)}
                />
                <Text style={styles.alert}>{usernameError}</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    secureTextEntry={true}
                    value={password}
                    onChangeText={(text) => setPassword(text)}
                />
                <Text style={styles.alert}>{passwordError}</Text>

                <Text style={styles.alert1}>{error}</Text>

                <TouchableOpacity style={styles.button}>
                    <Text style={styles.text_button} onPress={handleLogin}>
                        {" "}
                        LOGIN
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigation.push("ForgotPassword")}
                >
                    <Text style={styles.forgot}>Forgot your password?</Text>
                </TouchableOpacity>

                <View style={styles.text}>
                    <Text style={styles.text1}>Don't have an account? </Text>
                    <Text
                        style={styles.text2}
                        onPress={() => navigation.navigate("SignUp")}
                    >
                        Sign up
                    </Text>
                </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        textAlign: "center",
    },
    logo: {
        flex: 1,
    },
    image: {
        width: "100%",
    },
    title: {
        color: "#F2DEBA",
        fontWeight: "bold",
        marginTop: 190,
        fontSize: 30,
        marginBottom: 100,
    },
    input: {
        height: 40,
        width: "70%",
        borderWidth: 1,
        padding: 10,
        margin: 10,
        borderRadius: 15,
        marginLeft: "15%",
        backgroundColor: "#FFEFD6",
    },
    forgot: {
        color: "#EF9A53",
        marginBottom: 10,
        textAlign: "center",
    },
    button: {
        backgroundColor: "#EF9A53",
        width: "70%",
        padding: 8,
        marginTop: 30,
        borderRadius: 5,
        marginLeft: "15%",
        marginBottom: 90,
    },
    text_button: {
        color: "white",
        fontWeight: 500,
    },
    text: {
        flexDirection: "row",
        alignSelf: "center",
    },
    text1: {
        color: "#FDFDBD",
    },
    text2: {
        color: "#EF9A53",
        fontWeight: 600,
    },
    alert: {
        color: "#FFEFD6",
    },
    alert1: {
        marginTop: 15,
        color: "#FFEFD6",
        padding: 0,
    },
    header: {
        flexDirection: "row",
        width: "100%",
        paddingTop: 10,
        paddingBottom: 10,
    },
    imageBack: {
        width: 24,
        height: 24,
    },
});

export default LogIn;
