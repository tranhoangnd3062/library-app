import { useState } from 'react';
import { View, ImageBackground, Text, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';
import authApi from '../../api/AuthService';

const image = { uri: require('../../../assets/main.jpg') }

function SignUp({ navigation }) {

    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [usernameError, setUsernameError] = useState();
    const [passwordError, setPasswordError] = useState();
    const [emailError, setEmailError] = useState();
    const [phoneError, setPhoneError] = useState();
    const [error, setError] = useState();
    const [error1, setError1] = useState();

    async function handleRegister() {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        setError('');
        setError1('');
        setUsernameError('');
        setPasswordError('');
        setEmailError('');
        setPhoneError('');
        if (!username && !password && !email && !phone) {
            setUsernameError('Username cannot be empty!');
            setPasswordError('Password cannot be empty!');
            setEmailError('Email cannot be empty!');
            setPhoneError('Phone cannot be empty!');
        } else if (email && reg.test(email) === false) {
            setEmailError('Wrong format!')
        } else if (phone && phone.length !== 10) {
            setPhoneError('Phone number must be least 10 characters!')
        } else if (password && password.length < 6) {
            setPasswordError('Password must be least 6 characters!')
        } else if (!username || !password || !email || !phone) {
            setError1('Input fields cannot be empty!');
        } else {
            try {
                let data = {
                    user_name: username,
                    user_password: password,
                    email: email,
                    phone_number: phone,
                }

                const response = await authApi.register(data);

                if (response.status === 201)
                {
                    localStorage.setItem("user", JSON.stringify(response.data));
                    const token = localStorage.getItem("user");
                    const tokenString = JSON.parse(token);
                    localStorage.setItem("accessToken", tokenString.token);
                    window.location.reload();
                    if (response.data.user_type == 2)
                        navigation.replace('HomeScreenUser');
                    else 
                        navigation.navigate('HomeScreenAdmin')
                }
            } catch {
                if (!error1) {
                    setError('Wrong information!')
                } else {
                    setError(error1);
                }    
            } 
        }
    }

    return (
        <View style={styles.container}>
            
            <ImageBackground style={styles.logo} resizeMode="cover" source={image}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                        <View style={{ flexDirection: 'row', marginLeft: 5, backgroundColor: '#EF9A53',}}>
                            <Image style={styles.imageBack} source={{ uri: 'https://cdn1.iconfinder.com/data/icons/duotone-essentials/24/chevron_backward-512.png' }}/>
                            <Text style={{ fontSize: 17, paddingRight: 5, color: 'white', fontWeight: 500 }}>Back</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={styles.title}>Create Account</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Username"
                    value={username}
                    onChangeText={(text) => setUsername(text)}
                />
                <Text style={styles.alert}>{usernameError}</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    value={email}
                    onChangeText={(text) => setEmail(text)}
                />
                <Text style={styles.alert}>{emailError}</Text>

                <TextInput
                    style={styles.input}
                    placeholder="PhoneNumber"
                    value={phone}
                    onChangeText={(text) => setPhone(text)}
                    maxLength='10'
                />
                <Text style={styles.alert}>{phoneError}</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    secureTextEntry={true}
                    value={password}
                    onChangeText={(text) => setPassword(text)}
                />
                <Text style={styles.alert}>{passwordError}</Text>

                <Text style={styles.alert1}>{error || error1}</Text>

                <TouchableOpacity style={styles.button}>
                    <Text style={styles.text_button} onPress={handleRegister}>SIGN UP</Text>
                </TouchableOpacity>
                <View style={styles.text}>
                    <Text style={styles.text1}>Already have an account? </Text>
                    <Text style={styles.text2} onPress={() => navigation.navigate('LogIn')}>Login</Text>
                </View>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        textAlign: 'center',
    },
    logo: {
        flex: 1,
    },
    image: {
        width: "100%"
    },
    title: {
        color: "#F2DEBA",
        fontWeight: 'bold',
        marginTop: 190,
        fontSize: 30,
        marginBottom: 100,
    },
    input: {
        height: 40,
        width: '70%',
        borderWidth: 1,
        padding: 10,
        margin: 10,
        borderRadius: 5,
        marginLeft: "15%",
        backgroundColor: '#FFEFD6'
    },
    error: {
        color: '#FFEFD6',
    },
    button: {
        backgroundColor: '#EF9A53',
        width: '70%',
        padding: 8,
        marginTop: 30,
        borderRadius: 5,
        marginLeft: "15%",
        marginBottom: 90,
    },
    text_button: {
        color: 'white',
        fontWeight: 500,
    },
    text: {
        flexDirection: 'row',
        alignSelf: 'center'
    },
    text1: {
        color: '#FDFDBD'
    },
    text2: {
        color: '#EF9A53',
        fontWeight: 600,
    },
    alert: {
        color: '#FFEFD6',
    },
    alert1: {
        marginTop: 15,
        color: '#FFEFD6',
        padding: 0,
    },
    header: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
    },
    imageBack: {
        width: 24,
        height: 24,
    }
});

export default SignUp;